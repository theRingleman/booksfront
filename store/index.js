export const state = () => ({
    user: null,
    token: ""
});

export const getters = {
    loggedIn: state => {
        return !!state.token;
    },
};

export const mutations = {
    setToken (state, token) {
        state.token = token;
    },

    removeToken (state) {
        state.token = "";
        localStorage.removeItem('token');
    },

    setUser (state, user) {
        state.user = user;
    }
};

export const actions = {
    async getUser({ state, commit }) {
        const response = await this.$axios({
            url: 'https://theringleman.info/api/auth/user',
            method: 'get',
            headers: {
                'X-Authorization': `Bearer ${state.token}`
            }
        });
        commit('setUser', response.data);
    },
    async logout({ state, commit }) {
        try {
            await this.$axios({
                url: 'https://theringleman.info/api/auth/logout',
                method: 'get',
                headers: {
                    'X-Authorization': `Bearer ${state.token}`
                }
            });
            commit('removeToken');
        } catch (error) {
            commit('removeToken');
        }
    }
};